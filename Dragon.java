import java.util.Scanner;

public class Dragon{
	private String color;
	private int wingSpan;
	private boolean breathesFire;
	private String behavior;
	private String name;

	public Dragon(String color, int wingSpan, boolean breathesFire, String behavior, String name){
		this.color = color;
		this.wingSpan = wingSpan;
		this.breathesFire = breathesFire;
		this.behavior = behavior;
		this.name = name;
	}
	
	public String getColor(){
		return this.color;
	}
	public int getWingSpan(){
		return this.wingSpan;
	}
	public boolean getBreathesFire(){
		return this.breathesFire;
	}
	public String getBehavior(){
		return this.behavior;
	}
	public String getName(){
		return this.name;
	}
	
	public void setColor(String color){
		this.color = color;
	}
	public void setWingSpan(int wingSpan){
		this.wingSpan = wingSpan;
	}
	public void setBreathesFire(boolean breathesFire){
		this.breathesFire = breathesFire;
	}
	public void setBehaviour(String behaviour){
		this.behavior = behavior;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public void flightSpeed(){
		Scanner reader = new Scanner(System.in);
		System.out.println("How many hours has your dragon flied for?");
		int hrs = Integer.parseInt(reader.nextLine());
		// here, the wingspan in meters gets converted into km/h
		// for each meter of wingspan, the dragon can fly 2 m/s
		float speed = (int)((float)this.wingSpan * (float)7.2);
		float distance = speed * hrs;
		System.out.println("Your dragon, " + name +  " has traveled a distance of " + distance + " km over " + hrs + " hours.");
		if(hrs >= 24) System.out.println("Stop abusing your dragon, it's been flying for over a day.");
	}
	
	// 2. print out the dragon's reaction when you try to pet it.
	public void petDragon(){
		System.out.println("Petting dragon...");
		String output = "Your " + this.color + " dragon, " + this.name;
		if(this.behavior.equals("sleepy")){
			output += ", turns over in its sleep.";
		} else if(this.behavior.equals("passive")){
			output += ", waggles its tail.";
		} else if(this.behavior.equals("aggressive")){
			output += ", tries to blow fire in your direction...";
			if(!breathesFire) output += " Fortunately, " + name + " doesn't breathe fire.";
			else output += " Your head is on fire! Who said it was a great idea to pet a dragon?";
		} else{
			output += ", flies away.";
		}
		System.out.println(output);
	}
}