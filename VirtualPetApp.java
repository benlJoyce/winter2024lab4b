import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		Dragon[] dragons = new Dragon[4];
		
		Scanner reader = new Scanner(System.in);
		for(int i = 0; i < dragons.length; i++){
			
			System.out.println("Dragon #" + (i + 1) + ":");
			
			System.out.println("What's the name of your dragon?");
			String name = reader.nextLine();
			
			System.out.println("What's the wingspan of your dragon, in meters?");
			int wingSpan = Integer.parseInt(reader.nextLine());
			
			System.out.println("Can your dragon breathe fire? Answer with y/n (y for yes, n for no)");
			String fire = reader.nextLine();
			while((fire.charAt(0) != 'n' || fire.charAt(0) != 'y') && fire.length() != 1){
				System.out.println("Please provide a valid answer");
				fire = reader.nextLine();
			}
			boolean breathesFire = false;
			if(fire.charAt(0) == 'y') breathesFire = true;
			
			System.out.println("What color is your dragon?");
			String color = reader.nextLine();
			
			System.out.println("What kind of behavior does your dragon have? \nChoices: aggressive, sleepy, passive");
			String behavior = reader.nextLine();
			
			dragons[i] = new Dragon(color, wingSpan, breathesFire, behavior, name);
		
		}
		System.out.println("CHARACTERISTICS OF DRAGON:");
		System.out.println(dragons[dragons.length-1].getName());
		System.out.println(dragons[dragons.length-1].getColor());
		System.out.println(dragons[dragons.length-1].getWingSpan());
		System.out.println(dragons[dragons.length-1].getBreathesFire());
		System.out.println(dragons[dragons.length-1].getBehavior());
		
		System.out.println("Give new color for last dragon");
		String newColor = reader.nextLine();
		dragons[dragons.length-1].setColor(newColor);
		
		System.out.println("CHARACTERISTICS OF DRAGON:");
		System.out.println(dragons[dragons.length-1].getName());
		System.out.println(dragons[dragons.length-1].getColor());
		System.out.println(dragons[dragons.length-1].getWingSpan());
		System.out.println(dragons[dragons.length-1].getBreathesFire());
		System.out.println(dragons[dragons.length-1].getBehavior());
		
	}
}